import React from 'react';
import toast, {Toaster} from 'react-hot-toast';
import nookies, {parseCookies} from "nookies"
import { useNavigate, Link } from 'react-router-dom';
import { getUserDataFromToken } from '../Auth/Service';

const Navbar = () => {
  const navigate = useNavigate()

  const { role } = getUserDataFromToken()

    const handleLogout = async () => {
        const cookies = parseCookies()
        const accessToken = cookies.accessToken

        try {
            toast.loading("sending request...")

            /* const response = await fetch("http://35.208.176.39/api/oauth/logout", {
                method: "POST",
                headers: {
                    "Authorization": "Bearer " + accessToken,
                }          
            })

            if (response.ok === false) {
                const responseData = response.json()
                const message = responseData.message
                throw new Error(message)
            } else {
                nookies.destroy(null, "accessToken")
                toast.dismiss()
                toast.success("logout successful")
                navigate("/login")
            } */
            nookies.destroy(null, "accessToken")
            toast.dismiss()
            toast.success("logout successful")
            navigate("/login")
        } catch (error) {
            toast.dismiss()
            toast.error(error.message)
        }
    }

    return (
        <>
          <Toaster className="z-10"/>
          <nav className="w-full flex justify-around py-5 bg-gray-100 shadow font-semibold text-gray-700">
              <div>
                  <h3 className="text-2xl font-semibold">PizzaKu</h3>
              </div>
              <div className="xs:space-x-8 space-x-5">
                  {role === "customer" ? <Link to="/order">Menu</Link> : <div></div>}
                  <Link to="/status-pemesanan">Status Pesanan</Link>
                  <button onClick={handleLogout} className="font-semibold text-gray-700">Logout</button>
              </div>
          </nav>
        </>
      );
    }

export default Navbar;
