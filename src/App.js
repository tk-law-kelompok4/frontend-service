import React from 'react'
import { BrowserRouter as Router, Route, Routes, Outlet } from "react-router-dom"

import Navbar from './Navbar/Navbar';
import Home from './Home/Home';
import Register from './Auth/Register';
import Login from './Auth/Login';
import StatusPemesanan from './StatusPemesanan';
import NotFound from './Error/NotFound';
import useLoginStatus from "./Auth/Service";
import Order from './Order/Order';
import DetailOrder from './Order/DetailOrder';
import './App.css';
import { QueryClient, QueryClientProvider } from 'react-query';

const queryClient = new QueryClient()

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <Routes>
          <Route element={<Layout />}>
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/order" element={<Order />} />
            <Route path="/detail-order/:id" element={<DetailOrder />}/>
          </Route>
          <Route element={<LayoutWithNavbar />}>
            <Route path="/" element={<Home />} />
            <Route path="/status-pemesanan" element={<StatusPemesanan />} />
            <Route path="*" element={<NotFound />} />
          </Route>
        </Routes>
      </Router>
    </QueryClientProvider>
  );
}

function LayoutWithNavbar() {
  useLoginStatus()

  return (
    <div className="App">
      <Navbar />
      <Outlet />
    </div>
  )
}

function Layout() {
  return (
    <div className="App">
      <Outlet />
    </div>
  )
}

export default App;
