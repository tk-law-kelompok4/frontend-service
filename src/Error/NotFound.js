import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => {
    return (
        <div className="flex flex-col items-center">
            <h1 className="text-3xl my-10">Page not found</h1>
            <Link to="/" className="text-blue-500">Back to home</Link>
        </div>
    );
  }
  
export default NotFound;
  
