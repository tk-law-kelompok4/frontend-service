import React from 'react';
import { useNavigate, Link } from 'react-router-dom';
import toast, {Toaster} from 'react-hot-toast';
import nookies from "nookies"

const Login = () => {
    const navigate = useNavigate()

    const submitForm = async e => {
        e.preventDefault()

        let usernameInput = document.getElementById("username").value
        let passwordInput = document.getElementById("password").value

        if (usernameInput === "" || passwordInput === "") {
            toast.dismiss()
            toast.error("please fill in all input fields")
        } else {
            const data = {
                "username": usernameInput,
                "password": passwordInput
            }
            
            try {
                toast.loading("sending request...")

                const response = await fetch("http://35.208.176.39/api/oauth/login", {
                    method: "POST",
                    headers: {
                    "Content-Type": "application/json",
                    },
                    body: JSON.stringify(data),
                })
            
                const responseData = await response.json()

                if (!response.ok) {
                    const message = responseData.message
                    throw new Error(message)
                } else {
                    const accessToken = responseData.accessToken
                    nookies.set(null, "accessToken", accessToken)
                    toast.dismiss()
                    toast.success("login successful")
                    navigate("/")
                }
            } catch (error) {
                toast.dismiss()
                toast.error(error.message)
            }
        }
    }

    return (
        <div className="w-full h-screen flex flex-col justify-center items-center bg-gray-100 px-5">
            <Toaster />
            <h1 className="block text-gray-700 text-sm font-bold text-2xl mb-5">Login</h1>
            <form className="w-full md:w-1/2 flex flex-col gap-4 bg-white shadow-md rounded p-8">
                <div>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Username"/>
                </div>
                <div>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" placeholder="Password"/>
                </div>
                <div className="mt-10">
                    <button onClick={submitForm} className="w-full bg-gray-800 hover:bg-gray-900 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button">
                        Submit
                    </button>
                </div>
                <div>
                    <span>Not registered? </span><Link to="/register" className="text-blue-500">Register here</Link>
                </div>
            </form>
        </div>
    );
  }
  
  export default Login;
  
