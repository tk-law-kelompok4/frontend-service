import { useEffect } from "react";
import { useNavigate } from 'react-router-dom'
import { parseCookies } from "nookies"
import jwtDecode from "jwt-decode";

export default function useLoginStatus() {
  const navigate = useNavigate()

  const cookies = parseCookies()
  const accessToken = cookies.accessToken

  useEffect(() => {
    if (!accessToken) {
      return navigate("/login")
    }
  },[accessToken]);
}

export function getUserDataFromToken() {
  const accessToken = parseCookies().accessToken
  if (!accessToken) {
    return {}
  }

  const { username, role } = jwtDecode(accessToken)
  return { username, role }
}