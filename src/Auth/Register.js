import React from 'react';
import { useNavigate, Link } from 'react-router-dom';
import toast, { Toaster } from 'react-hot-toast';


const Register = () => {
    const navigate = useNavigate()

    const submitForm = async e => {
        e.preventDefault()

        let usernameInput = document.getElementById("username").value
        let passwordInput = document.getElementById("password").value
        let roleInput = document.getElementById("role").value

        if (usernameInput === "" || passwordInput === "") {
            toast.dismiss()
            toast.error("please fill in all input fields")
        } else if (roleInput === "Role") {
            toast.dismiss()
            toast.error("please select a role")
        } else {
            const data = {
                "username": usernameInput,
                "password": passwordInput,
                "role": roleInput
            }
            
            try {
                toast.loading("sending request...")

                const response = await fetch("http://35.208.176.39/api/oauth/register", {
                    method: "POST",
                    headers: {
                    "Content-Type": "application/json",
                    },
                    body: JSON.stringify(data),
                })
            
                if (response.ok === false) {
                    const responseJSON = await response.json()
                    const message = responseJSON.message
                    throw new Error(message)
                } else {
                    toast.dismiss()
                    toast.success("registration successful, please login")
                    navigate("/login")
                }
            } catch (error) {
                toast.dismiss()
                toast.error(error.message)
            }
        }
    }
    
  return (
    <div className="w-full h-screen flex flex-col justify-center items-center bg-gray-100 px-5">
            <Toaster />
            <h1 className="block text-gray-700 text-sm font-bold text-2xl mb-5">Register</h1>
            <form className="w-full md:w-1/2 flex flex-col gap-4 bg-white shadow-md rounded p-8">
            <div>
                <input className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Username"/>
            </div>
            <div>
                <input className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" placeholder="Password"/>
            </div>
            <div>
                <select className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline" id="role">
                    <option disabled selected>Role</option>
                    <option value="customer">Customer</option>
                    <option value="chef">Chef</option>
                </select>
            </div>
            <div className="mt-10">
                <button onClick={submitForm} className="w-full bg-gray-800 hover:bg-gray-900 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button">
                    Submit
                </button>
            </div>
            <div>
                <span>Already registered? </span><Link to="/login" className="text-blue-500">Login here</Link>
            </div>
        </form>
    </div>
  );
}

export default Register;
