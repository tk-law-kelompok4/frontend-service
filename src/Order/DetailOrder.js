import React, { useEffect, useState, useRef } from 'react';
import { useParams } from 'react-router-dom';
import toast from 'react-hot-toast';
import { parseCookies } from "nookies";
import { useMutation, useQueryClient, useQuery } from 'react-query'

import Navbar from "../Navbar/Navbar";
import useLoginStatus from '../Auth/Service';

async function payByOrderId({ orderId }) {
  const token = parseCookies().accessToken
  return await fetch("http://35.208.176.39/api/transaction", {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ orderId })
    })
    .then(res => res.json())
}

function BayarTransaksi({ orderId }) {
  const toastTimeoutRef = useRef(null)
  useEffect(() => {
    return () => clearTimeout(toastTimeoutRef.current)
  })

  const queryClient = useQueryClient()
  const transactionMutation = useMutation(payByOrderId, {
    onSuccess() {
      queryClient.invalidateQueries('orders')
      toast.dismiss()
      toast.success('transaction successfull')
    },
    onError() {
      toast.dismiss()
      toast.error('transaction failed')
    },
    onSettled() {
      toastTimeoutRef.current = setTimeout(toast.dismiss, 2000)
    }
  })

  function handleTransaksi(e) {
    console.log('yay')
    e.preventDefault()
    transactionMutation.mutate({ orderId })
  }

  return <button onClick={handleTransaksi} className='bg-white hover:bg-gray-100 text-gray-800 font-semibold px-3 border border-gray-400 rounded shadow mx-3' type="submit">Bayar</button>
}

const fetchDetailOrder = async (orderId) => {
  const token = parseCookies().accessToken
  return await fetch(`http://35.208.176.39/api/orders/${orderId}`, {
    method: "GET",
    headers: {
      "Authorization": `Bearer ${token}`
    }
  }).then(res => res.json())
}

function DetailOrderTable({ data, isLoading, isError, error, id }) {
  if (isLoading) {
    return <p>Loading...</p>
  }

  if (isError) {
    return <p>Error: {error}</p>
  }

  const order = data.items
  const orderStatus = data.orderStatus
  
  let total = 0
  order.forEach(menu => {
    total += Number.parseFloat(menu.price)
  });

  let disabled = true;
  if (orderStatus === "PENDING_PAYMENT") disabled = false

  return (<table className="min-w-full" style={{ width: '100%' }}>
    <thead className="border-b">
      <tr>
        <th className="text-left" style={{ width: '40%' }}>Jenis Pizza</th>
        <th style={{ width: '30%' }}>Harga Satuan</th>
        <th style={{ width: '30%' }}>Jumlah</th>
      </tr>
    </thead>
    <tbody>
      {order.length ? order.map((menu) => (
        <tr key={menu.id} className="border-b py-8">
          <td className="text-left">{menu.name}</td>
          <td className="text-center">Rp {menu.price}</td>
          <td className="text-center">{menu.quantity}</td>
        </tr>
      )) : <p>There are no order</p>}
      <tr className="py-8">
        <td className="text-left">Total</td>
        <td className="text-center">Rp {total}</td>
        <td className="text-center">
          {!disabled && <BayarTransaksi orderId={id} />}
        </td>
      </tr>
    </tbody>
  </table>)
}

const DetailOrder = () => {
  const { id } = useParams()
  const { isLoading, isError, data, error } = useQuery(['order-detail', id], () => fetchDetailOrder(id))

  useLoginStatus()

  return (
    <>
      <Navbar />
      <div className="container mx-auto px-20 py-8 text-lg leading-loose">
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
            <div className="overflow-hidden">
              <DetailOrderTable data={data} isLoading={isLoading} isError={isError} error={error} id={id} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default DetailOrder;
