import React, { useEffect, useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import toast, { Toaster } from "react-hot-toast";
import { parseCookies } from "nookies";

import Navbar from "../Navbar/Navbar";
import useLoginStatus, { getUserDataFromToken } from "../Auth/Service";


const fetchMenus = async (token) => {
  const res = await fetch("http://35.208.176.39/api/menu", {
    method: "GET",
    headers: {
      "Authorization": `Bearer ${token}`
    }
  })
  const data = await res.json()
  return createQuantityMenu(data);
};

function createQuantityMenu(menu) {
  return menu.items.map((item) => ({ quantity: 0, ...item }));
}

function createOrders(menus) {
  return menus.filter((menu) => {
    if (menu.quantity > 0) {
      return menu
    }
  });
}

function cleanOrders(menus) {
  return menus.map(({ price, ...rest }) => rest);
}

const Order = () => {
  const navigate = useNavigate();
  useLoginStatus();

  const token = parseCookies().accessToken;
  const [menu, setMenu] = useState([]);
  const { role } = getUserDataFromToken()

  if (role === "chef") {
    toast.dismiss();
    toast.error("Role must be customer")
    navigate("/")
  }

  useEffect(() => {
    fetchMenus(token)
      .then((items) => setMenu(items))
      .catch(console.error);
  }, [token]);
  function updateItemQuantity(itemName, quantity) {
    if (quantity === -1) {
      return;
    }

    setMenu((menu) =>
      menu.map((item) => {
        if (itemName === item.name) {
          return { ...item, quantity };
        }
        return item;
      })
    );
  }

  const submitForm = async (e) => {
    e.preventDefault();
    var orders = createOrders(menu)
    orders = cleanOrders(orders)
    console.log(orders);
    if (orders.length===0) {
      toast.dismiss()
      toast.error("please order something first")
    } else {
      try {
        toast.loading("sending request...");
   
        const response = await fetch("http://35.208.176.39/api/orders", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`,
          },
          body: JSON.stringify({items: orders}),
        });
   
        const responseData = await response.json();
   
        if (!response.ok) {
          const message = responseData.message;
          throw new Error(message);
        } else {
          toast.dismiss();
          toast.success("Order success");
          navigate("/status-pemesanan");
        }
       } catch (error) {
        toast.dismiss();
        toast.error(error.message);
       }
    }
  };

  return (
    <>
      <Navbar />
      <div className="container mx-auto px-20 py-8 text-lg leading-loose">
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
            <div className="overflow-hidden">
              <table className="min-w-full" style={{ width: "100%" }}>
                <thead className="border-b">
                  <tr>
                    <th className="text-left" style={{ width: "40%" }}>
                      Jenis Pizza
                    </th>
                    <th className="text-left" style={{ width: "30%" }}>
                      Harga Satuan
                    </th>
                    <th style={{ width: "30%" }}>Jumlah</th>
                  </tr>
                </thead>
                <tbody>
                  {menu.length !== 0 ? (
                    menu.map(({ name, price, quantity }) => (
                      <tr className="border-b py-8" key={name}>
                        <td className="text-left">{name}</td>
                        <td>Rp {price}</td>
                        <td className="flex justify-center content-center">
                          <button
                            type="button"
                            className="bg-white hover:bg-gray-100 text-gray-800 font-semibold px-3 border border-gray-400 rounded shadow mx-3"
                            onClick={() =>
                              updateItemQuantity(name, quantity + 1)
                            }
                          >
                            +
                          </button>
                          <p className="inline-block align-middle">
                            {quantity}
                          </p>
                          <button
                            type="button"
                            className="bg-white hover:bg-gray-100 text-gray-800 font-semibold px-3 border border-gray-400 rounded shadow mx-3"
                            onClick={() =>
                              updateItemQuantity(name, quantity - 1)
                            }
                          >
                            -
                          </button>
                        </td>
                      </tr>
                    ))
                  ) : (
                    <p>No menu found</p>
                  )}
                </tbody>
              </table>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <form>
                  <button
                    disabled={!menu.length}
                    type="button"
                    onClick={submitForm}
                    className="bg-white hover:bg-gray-100 text-gray-800 font-semibold px-3 border border-gray-400 rounded shadow mx-8 my-5"
                  >
                    Pesan
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Order;
