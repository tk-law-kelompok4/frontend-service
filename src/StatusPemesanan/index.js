import { parseCookies } from "nookies"
import toast from "react-hot-toast"
import { getUserDataFromToken } from "../Auth/Service"
import { useQuery, useQueryClient, useMutation } from 'react-query'
import { useEffect, useRef } from "react"
import { useNavigate } from 'react-router-dom'

async function fetchOrders() {
  const token = parseCookies().accessToken
  const res = await fetch("http://35.208.176.39/api/orders", {
    headers: {
      "Authorization": `Bearer ${token}`
    }
  })
  return res.json()
}

export default function StatusPemesanan() {
  const { isLoading, isError, data, error } = useQuery('orders', fetchOrders)
  const { role } = getUserDataFromToken()

  return (
    <div className="p-5 flex flex-col items-center">
      <h1 className="text-center">Status Pemesanan</h1>
      <StatusPemesananTable orders={data} role={role} isLoading={isLoading} isError={isError} error={error}/>
    </div>
  )
}

function getHeadersByRole(role) {
  if (role === "customer") {
    return ["Order ID", "Status", "Progress"]
  } else if (role === "chef") {
    return ["Order ID", "Status"]
  } else {
    return []
  }
}

function getCustomerStatusByStatusCode(statusCode, orderId) {
  switch (statusCode) {
    case "PENDING_PAYMENT":
      return "Menunggu Pembayaran"
    case "PENDING_APPROVAL":
      return "Menunggu Konfirmasi"
    case "COOKING":
      return "Dimasak"
    case "SENDING":
      return "Dikirim"
    case "FINISHED":
      return "Selesai"
    default:
      return ""
  }
}

async function approveByOrderId({ orderId }) {
  const token = parseCookies().accessToken
  return await fetch(`http://35.208.176.39/api/orders/${orderId}`, {
      method: 'PUT',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ status: "cooking" })
    })
    .then(res => res.json())
}

function TerimaPesanan({ orderId }) {
  const toastTimeoutRef = useRef(null)
  useEffect(() => {
    return () => clearTimeout(toastTimeoutRef.current)
  }, [])

  const queryClient = useQueryClient()
  const approveMutation = useMutation(approveByOrderId, {
    onSuccess() {
      queryClient.invalidateQueries('orders')
      toast.dismiss()
      toast.success('transaction successfull')
    },
    onError() {
      toast.dismiss()
      toast.error('transaction failed')
    },
    onSettled() {
      toastTimeoutRef.current = setTimeout(toast.dismiss, 2000)
    }
  })

  function handleTransaksi(e) {
    e.preventDefault()
    approveMutation.mutate({ orderId })
  }

  return <button onClick={handleTransaksi}>Terima Pesanan</button>
}

function getChefStatusByStatusCode(statusCode, orderId) {
  switch (statusCode) {
    case "PENDING_APPROVAL":
      return <TerimaPesanan orderId={orderId} />
    case "COOKING":
      return "Sudah Diterima"
    default:
      return ""
  }
}

function getStatus(statusCode, role, orderId) {
  if (role === "customer") {
    return getCustomerStatusByStatusCode(statusCode, orderId)
  } else if (role === "chef") {
    return getChefStatusByStatusCode(statusCode, orderId)
  }
}

function StatusPemesananTable({ orders, role, isLoading, isError, error }) {
  const navigate = useNavigate()

  if (isLoading) {
    return <p>Loading...</p>
  }
  
  if (isError) {
    return <p>Error: {error.messages}</p>
  }

  const headers = getHeadersByRole(role)

  function handleOrderDetail(orderId) {
    navigate(`/detail-order/${orderId}`)
  }

  return (
    <div className="w-3/4 min-w-0 max-w-2xl">
      <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
          <div className="overflow-hidden">
            <table className="min-w-full">
              <thead className="border-b">
                <tr>
                  {headers.map(header => (
                    <th scope="col" key={header} className="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                      {header}
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody>
                {orders.data.map(({id, orderStatus }) => (
                  <tr className="bg-white border-b" key={id}>
                    <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                      <button onClick={() => handleOrderDetail(id)}>{id}</button>
                    </td>
                    <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                      {getStatus(orderStatus, role, id)}
                    </td>
                    {role === "customer" ? (
                      <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                        {orderStatus === "COOKING" || orderStatus === "SENDING" ? (
                          <div className="w-full bg-gray-200 rounded-full h-2.5 dark:bg-gray-700">
                            <div className="bg-gray-600 h-2.5 rounded-full dark:bg-gray-300" style={{width: "45%"}}></div>
                          </div>
                        ) : null}
                      </td>
                    ) : null}
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}
